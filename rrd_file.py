import rrdtool


def write_to_file(data):
    with open('out.txt', 'a') as my_file:
        my_file.write(data)
        my_file.write('\n')

file_name = 'comp-129d-batt2_temp1_62269-1.rrd'
output = rrdtool.fetch([file_name, 'MAX', '-s', str(rrdtool.first(file_name)), '-e', str(rrdtool.last(file_name))])
print('Start Index', str(rrdtool.first(file_name)))
print('End Index', str(rrdtool.last(file_name)))
output = output[2]
no_of_tuples = len(output)
print('No of tuples: ', no_of_tuples)
min_value = 10000.0
open('out.txt', 'w').close()


for index,each_output in enumerate(output):
    write_sentence = ' , '.join(str(item) for item in each_output)
    write_to_file(write_sentence) if len(write_sentence) > 0 else write_to_file('Empty Sentence')
    for each_entry in each_output:
        try:
            if float(each_entry) < min_value:
                print('Entry: %s , Index: %s'%(each_entry, index+1))
                min_value = float(each_entry)
        except:
            pass

print(index+1)
